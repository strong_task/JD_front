function setCookie(Key, value, days) {
	
    var d = new Date;

    d.setTime(d.getTime() + 24*60*60*1000*days);
	
	
    window.document.cookie = Key + "=" + value + ";path=/;expires=" + d.toGMTString();

}

 function getCookie(Key) {
	
    var v = window.document.cookie.match('(^|;) ?' + Key + '=([^;]*)(;|$)');

    return v ? v[2] : null;

}

 function deleteCookie(Key) {

    this.set(Key, '', -1);

}
 
