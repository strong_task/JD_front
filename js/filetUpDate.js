//文件上传




UpDate: function() {
		var inputDOM = this.$refs.inputer;
		// 通过DOM取文件数据
		this.file = inputDOM.files[0];
		console.log("inputDOM.files[0]:" + inputDOM.files[0]);
		if(inputDOM.files[0] === undefined) {
			this.fileName = '';
			this.fileSize = '';
			this.fileType = '';
		} else {
			this.fileName = this.file.name;
			//kb
			if(this.file.size / 1024 < 1000) {
				this.fileSize = (this.file.size / 1024).toFixed(2) + "kb";
			} else if(this.file.size / 1024 > 1000) {
				this.fileSize = ((this.file.size / 1024) / 1024).toFixed(2) + "MB";
			}
			this.fileType = this.file.type;
		}
	},

	//用新button按钮去替换原始的控件
	fileopen: function() {
		var fileSelect = document.getElementById("fileSelect");
		fileSelect.click();
	},

	//上传按钮的事件axios
	fileupdate: function(Url,Address) {
		var inputDOM = this.$refs.inputer;
		// 通过DOM取文件数据
		this.file = inputDOM.files[0];
		var formdata = new FormData();
		formdata.append('file', inputDOM.files[0]);

		axios({
				url: Url + Address,
				method: 'post',
				data: formdata,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then(function(re) {
				alert("添加成功");
				console.log(re);
			})
			.catch(function(err) {
				console.log(err);
			})
	}
//文件上传end