//*******************************
//			柱状图脚本
//*******************************
echarts.init(document.getElementById('histogram')).setOption({
    title: {
        text: '历年竞赛参与积极度分析'
    },
    tooltip: {},
    legend: {
        data: ['参与人数', '报名人数']
    },
    xAxis: {
        data: ["2014", "2015", "2016", "2017", "2018"]
    },
    yAxis: {},
    series: [{
        name: '参与人数',
        type: 'bar',
        data: [5, 20, 36, 10, 10]
    },
        {
            name: '报名人数',
            type: 'bar',
            data: [7, 23, 40, 15, 10]
        }
    ]
});

//*******************************
//			饼状图脚本
//*******************************
echarts.init(document.getElementById('pie')).setOption({
    title: {
        text: '历届就业率分析'
    },
    tooltip: {},
    legend: {
        data: ['2014年', '2015年', '2016年']
    },
    series: {
        type: 'pie',
        data: [{
            name: '2014年',
            value: 1212
        },
            {
                name: '2015年',
                value: 2323
            },
            {
                name: '2016年',
                value: 1919
            }
        ]
    }
});